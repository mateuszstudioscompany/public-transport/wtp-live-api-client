<?php
declare(strict_types=1);

namespace MSCO\WarsawPublicTransport\Api\LiveVehicles;

use DateTime;

class LiveVehicle
{
	protected array $vehicleNumber;

	protected string $line;

	protected int $brigade;

	protected float $lat;

	protected float $lon;

	protected DateTime $updateTime;

	/**
	 * Get vehicleNumber
	 */ 
	public function getVehicleNumbers(): array
	{
		return $this->vehicleNumber;
	}

	/**
	 * Set vehicleNumber
	 *
	 * @return  self
	 */ 
	public function setVehicleNumbers(array $vehicleNumber)
	{
		$this->vehicleNumber = $vehicleNumber;

		return $this;
	}

	/**
	 * Get line
	 */ 
	public function getLine(): string
	{
		return $this->line;
	}

	/**
	 * Set line
	 *
	 * @return  self
	 */ 
	public function setLine(string $line)
	{
		$this->line = $line;

		return $this;
	}

	/**
	 * Get brigade
	 */ 
	public function getBrigade(): int
	{
		return $this->brigade;
	}

	/**
	 * Set brigade
	 *
	 * @return  self
	 */ 
	public function setBrigade(int $brigade)
	{
		$this->brigade = $brigade;

		return $this;
	}

	/**
	 * Get lat
	 */ 
	public function getLat(): float
	{
		return $this->lat;
	}

	/**
	 * Set lat
	 *
	 * @return  self
	 */ 
	public function setLat(float $lat)
	{
		$this->lat = $lat;

		return $this;
	}

	/**
	 * Get lon
	 */ 
	public function getLon(): float
	{
		return $this->lon;
	}

	/**
	 * Set lon
	 *
	 * @return  self
	 */ 
	public function setLon(float $lon)
	{
		$this->lon = $lon;

		return $this;
	}

	/**
	 * Get updateTime
	 */ 
	public function getUpdateTime(): DateTime
	{
		return $this->updateTime;
	}

	/**
	 * Set updateTime
	 *
	 * @return  self
	 */ 
	public function setUpdateTime(DateTime $updateTime)
	{
		$this->updateTime = $updateTime;

		return $this;
	}
}