<?php
declare(strict_types=1);

namespace MSCO\WarsawPublicTransport\Api\LiveVehicles\Client;

use DateTime;
use DateTimeZone;
use Exception;
use MSCO\WarsawPublicTransport\Api\LiveVehicles\Enum\LiveVehicleType;
use MSCO\WarsawPublicTransport\Api\LiveVehicles\LiveVehicle;

class LiveVehiclesClient
{
	const API_URL = 'https://api.um.warszawa.pl/api/action/busestrams_get/';

	const API_RESOURCE_ID = 'f2e5503e-927d-4ad3-9500-4ab9e55deb59';

	const PARAM_LINE = 'line';

	const PARAM_TYPE = 'type';

	const PARAM_BRIGADE = 'brigade';

	protected string $apiKey;

	protected string $apiUrl;

	protected string $resourceId;

	public function __construct(
		string $apiKey,
		string $apiUrl = self::API_URL,
		string $resourceId = self::API_RESOURCE_ID
	) {
		$this->apiKey = $apiKey;
		$this->apiUrl = $apiUrl;
		$this->resourceId = $resourceId;
	}

	/**
	 * Get all vehicles with filters.
	 */
	public function getVehicles(
		$type = null,
		string $line = null,
		string $brigade = null,
		DateTime $minUpdateTime = null
	): array {

		if (is_null($type)) {
			return array_merge(
				$this->_getVehicles(LiveVehicleType::BUS, $line, $brigade, $minUpdateTime),
				$this->_getVehicles(LiveVehicleType::TRAM, $line, $brigade, $minUpdateTime)
			);
		} else {
			return $this->_getVehicles($type, $line, $brigade, $minUpdateTime);
		}
	}

	/**
	 * Get specified bus by line and brigade.
	 */
	public function getBus(string $line, string $brigade, DateTime $minUpdateTime = null): LiveVehicle
	{
		return $this->_getVehicles(LiveVehicleType::BUS, $line, $brigade, $minUpdateTime)[0];
	}

	/**
	 * Get specified tram by line and brigade.
	 */
	public function getTram(string $line, string $brigade, DateTime $minUpdateTime = null): LiveVehicle
	{
		return $this->_getVehicles(LiveVehicleType::TRAM, $line, $brigade, $minUpdateTime)[0];
	}

	// /**
	//  * Get vehicle from specified line (tries to detect line type).
	//  * Edgecase: If special tram line has  number outside of 1-79 will NOT be detected correctly!
	//  */
	// public function getLine(string $line, DateTime $minUpdateTime = null): array
	// {
	// 	$isTram = $this->isStandardTramLine($line);

	// 	if ($isTram) {
	// 		return $this->getTramLine($line, $minUpdateTime);
	// 	} else {
	// 		return $this->getBusLine($line, $minUpdateTime);
	// 	}
	// }
	
	/**
	 * Get buses from specified line.
	 */
	public function getBusLine(string $line, DateTime $minUpdateTime = null): array
	{
		return $this->_getVehicles(LiveVehicleType::BUS, $line, null, $minUpdateTime);
	}

	/**
	 * Get trams from specified line.
	 */
	public function getTramLine(string $line,DateTime $minUpdateTime = null): array
	{
		return $this->_getVehicles(LiveVehicleType::TRAM, $line, null, $minUpdateTime);
	}
	
	/**
	 * Get all vehicles with filters.
	 */
	protected function _getVehicles(
		$type,
		string $line = null,
		string $brigade = null,
		DateTime $minUpdateTime = null
	): array {

		$params = [
			self::PARAM_TYPE => $type
		];

		if (!is_null($line)) {
			$params[self::PARAM_LINE] = $line;
		}
		if (!is_null($brigade)) {
			$params[self::PARAM_BRIGADE] = $brigade;
		}

		$response = $this->sendRequest($params);
		$data = $this->parseResponse($response);
		$vehicles = [];

		foreach ($data as $vehicleData) {
			$updatedTime = new DateTime($vehicleData['Time'], new DateTimeZone('Europe/Warsaw'));
			$vehicleNumbers = array_map(
				function($el) { return intval($el); },
				explode('+', $vehicleData['VehicleNumber'])
			);

			if (!is_null($minUpdateTime) && $minUpdateTime > $updatedTime) {
				continue;
			}

			$vehicle = new LiveVehicle();
			$vehicle->setVehicleNumbers($vehicleNumbers)
				->setLine($vehicleData['Lines'])
				->setBrigade(intval($vehicleData['Brigade']))
				->setLat(floatval($vehicleData['Lat']))
				->setLon(floatval($vehicleData['Lon']))
				->setUpdateTime($updatedTime)
			;
			$vehicles[] = $vehicle;
		}

		return $vehicles;
	}

	protected function sendRequest($params): string
	{
		$params['resource_id'] = $this->resourceId;
		$params['apikey'] = $this->apiKey;

		$url = $this->apiUrl.'?'.http_build_query($params);

		$request = curl_init($url);
		curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($request, CURLOPT_HEADER, 0);

		$response = curl_exec($request);
		if ($response == false) {
			throw new Exception('curl failed');
		}

		$responseCode = curl_getinfo($request, CURLINFO_HTTP_CODE);
		if ($responseCode != 200) {
			throw new Exception('got error code: '.$responseCode);
		}

		curl_close($request);

		return $response;
	}

	protected function parseResponse(string $response)
	{
		$data = json_decode($response, true);
		$this->validateData($data);

		return $data['result'];
	}

	protected function validateData(array $data)
	{
		if (is_null($data)) {
			throw new Exception('cannot decode response');
		}

		if (isset($data['error'])) {
			throw new Exception($data['error']);
		}

		if (!isset($data['result']) || $data['result'] === 'false') {
			throw new Exception('result failed');
		}
	}

	/**
	 * Detect tram line.
	 */
	protected function isStandardTramLine(string $lineNumber)
	{
		if (!is_numeric($lineNumber)) {
			return false;
		}

		$lineNumber = intval($lineNumber);

		return ($lineNumber > 0 && $lineNumber < 80);
	}
}