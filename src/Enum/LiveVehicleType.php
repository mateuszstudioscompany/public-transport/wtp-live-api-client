<?php

namespace MSCO\WarsawPublicTransport\Api\LiveVehicles\Enum;

class LiveVehicleType
{
	const BUS = 1;

	const TRAM = 2;
}