# Overview
This is simple client for Warsaw Public Transport Live Bus & Trams Data.

**Also see [api limitations](##Limitations) and [api bugs](##Api-Bugs)!**

# How use
todo

# Api
## Docs
You can find offical docs [here](https://api.um.warszawa.pl/files/9fae6f84-4c81-476e-8450-6755c8451ccf.pdf) (in polish, pretty limited).

## Limitations
This limitations are based on docs and my observations. Docs are pretty limited,
so i might be wrong somewhere.

- you **have to** ask for buses or trams but you **cannot** ask for both at once.
- you **cannot** ask for multiple lines or brigades at once.

## Api Bugs
### Random zero padding in brigades
Afaik brigades are numbers but api returns ex. brigade 4 as `04` insted
of `4`.

So **while searching** with brigade you should search for both versions. I
cannot handle it due to [limitations](#Limitations)...

But in **results** (`LiveVehicle` object) `04` will be translated to numer `4`.

*If i'm wrong please correct me!*

# License and Warrianty
See license file.

# Thanks to
Thanks to [this forum](https://forum.kodujdlapolski.pl/t/geopozycje-warszawskich-autobusow-obecny-stan-danych/2771/4) for asking and providing info about this resource.
